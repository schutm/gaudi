(Mostly) current buildstatus:
[![Pipeline status](https://gitlab.com/schutm/macgyver/badges/master/build.svg)](https://gitlab.com/schutm/macgyver/commits/master)
[![Tests](https://schutm.gitlab.io/macgyver/shields/tests.svg)](https://schutm.gitlab.io/macgyver/tests)
[![Coverage](https://schutm.gitlab.io/macgyver/shields/coverage.svg)](https://schutm.gitlab.io/macgyver/coverage)
[![Style](https://schutm.gitlab.io/macgyver/shields/style.svg)](https://schutm.gitlab.io/macgyver/lint)
[![Build](https://schutm.gitlab.io/macgyver/shields/build.svg)](https://schutm.gitlab.io/macgyver/macgyver.latest.tar.gz)<br/>
Please note the above shields are cached, and may represent the state of an old build.

MacGyver
========
MacGyver is a collection of Swiss knife tools to make life easier when
developing in EcmaScript. It is a growing collection based on my needs,
but additions are always welcome.


Installation
------------
As you'll note I don't use [npm](https://npmjs.com) as it doesn't fit my
development process. This makes the installation a bit cumbersome. If
you create a proper `package.json` to submit this library to npm, I'll
happily [accept the pull-request](CONTRIBUTING.md). The current `package.json`
should suffice for most use cases.

1. Make sure you have to the latest version downloaded
   * [Download the latest release](https://github.com/schutm/macgyver/zipball/master).
   * Clone the repo:
     `git clone git://github.com/schutm/macgyver.git`.
2. Copy or move the relevant files from the `dist` directory to your own
   project.


Bug tracker
-----------
[Open a new issue](https://github.com/schutm/macgyver/issues) for bugs
or feature requests. Please search for existing issues first.

Bugs or feature request will be fixed in the following order, if time
permits:

1. It has a pull-request with a working and tested fix.
2. It is easy to fix and has benefit to myself or a broader audience.
3. It puzzles me and triggers my curiosity to find a way to fix.


Contributing
------------
Anyone and everyone is welcome to [contribute](CONTRIBUTING.md).


License
-------
This software is licensed under the [ISC License](LICENSE).
