# Make sure not to build in the source directory
ifeq ($(SRCDIR),$(OUTDIR))
TARGET = ../output
endif

TEST_DEPENDENCIES ?= $(DEPENDENCIES)
DIST_DEPENDENCIES ?= $(DEPENDENCIES)

.PRECIOUS: $(DEPDIR)%

% := %
, := ,

export PATH := /usr/local/share/.config/yarn/bin:$(PATH)

#=======================================================================================================================
# Macros
#=======================================================================================================================
define depends_on
	$(addprefix $(DEPDIR),$(1))
endef

define bundle_dist_file
$(2)$(1): $(DISTDIR)$(1)
	@echo "Copying '$$<' to '$$@'"
	@mkdir -p $$(@D)
	@cp $$< $$@
endef

define bundle_dist_files
$(1): $(addprefix $(2),$(3))

$(foreach DEPENDENCY,$(3),$(eval $(call bundle_dist_file,$(DEPENDENCY),$(2))))
endef

define bundle_packages
	$(foreach DEPENDENCY,$(3),$(eval $(call bundle_$(DEPENDENCY),$1,$2)))
endef

define fetch_file
	@mkdir -p $(2) && \
	cp -r $(patsubst file://%,%,$(1))/* $(2)
endef

define fetch_git
	@git clone -q -n -- $(patsubst git://%,https://%,$(firstword $(1))) $(2) && \
	cd $(2) && git checkout -q $(lastword $(1))
endef

define fetch_https
	@mkdir -p $(2) && \
	cd $@ && curl -sL $(1) | tar xj
endef


#=======================================================================================================================
# Dependencies
#=======================================================================================================================
DEP_REPO_macgyver = https://schutm.gitlab.io/macgyver/macgyver.latest.tar.bz2


DEP_REPO_mithril = git://github.com/MithrilJS/mithril.js.git next
MITHRIL = mithril.min.js
MITHRIL_BUNDLE = $(addprefix $(MITHRIL_BUNDLE_DIR),$(MITHRIL))
MITHRIL_BUNDLE_DIR=$(2)

$(addprefix $(DEPDIR)mithril/,$(MITHRIL)): | $(call depends_on,mithril) ;

define bundle_mithril
$(1): $(MITHRIL_BUNDLE)

$(MITHRIL_BUNDLE_DIR)$(MITHRIL): $(DEPDIR)mithril/$(MITHRIL)
	@echo "Copying '$$<' to '$$@'"
	@mkdir -p $$(@D)
	@cp $$< $$@
endef


DEP_REPO_material-icons = git://github.com/google/material-design-icons.git master
MATERIAL_ICONS = $(addprefix MaterialIcons-Regular.,eot woff woff2 ttf)
MATERIAL_ICONS_BUNDLE = $(addprefix $(MATERIAL_ICONS_BUNDLE_DIR),$(MATERIAL_ICONS) LICENSE.MaterialIcons)
MATERIAL_ICONS_BUNDLE_DIR=$(2)

$(addprefix $(DEPDIR)material-icons/iconfont/,$(MATERIAL_ICONS)): | $(call depends_on,material-icons) ;

define bundle_material-icons
$(1): $(MATERIAL_ICONS_BUNDLE)

$(MATERIAL_ICONS_BUNDLE_DIR)MaterialIcons-Regular.%: $(DEPDIR)material-icons/iconfont/MaterialIcons-Regular.%
	@echo "Copying '$$<' to '$$@'"
	@mkdir -p $$(@D)
	@cp $$< $$@

$(MATERIAL_ICONS_BUNDLE_DIR)LICENSE.MaterialIcons: $(DEPDIR)material-icons/LICENSE
	@echo "Copying '$$<' to '$$@'"
	@mkdir -p $$(@D)
	@cp $$< $$@
endef


export YARN_SILENT := 1
export NODE_PATH := $(DEPDIR):/usr/local/share/.config/yarn/global/node_modules/

.SUFFIXES:

find = $(patsubst $(1)/%,%,$(wildcard $(1)/$(2)) $(wildcard $(1)/**/$(2)))
yarn = yarn run $(1) $(2); EXIT_CODE=$$?; rm -rf node_modules; exit $${EXIT_CODE}

SRCS = $(addprefix src/,$(call find,$(SRCDIR)src,*.js))
TESTS = $(addprefix test/,$(call find,$(SRCDIR)test,*.js))


#=======================================================================================================================
# Clean generated files
#=======================================================================================================================
.PHONY: clean
clean:
	@rm -rf $(OUTDIR)

.PHONY: mostlyclean
mostlyclean:
	@find $(OUTDIR) -mindepth 1 -maxdepth 1 -type d ! -path $(DEPDIR:/=) -o -name .dist-dummy -exec rm -rf {} \;


#=======================================================================================================================
# Linting of the sources
#=======================================================================================================================
ifndef NO_LINT_TARGET
.PHONY: lint
lint:
	@$(call yarn,lint)

.PHONY: lint-ci
lint-ci: | $(abspath $(REPORTDIR)lint)/
	-@$(call yarn,lint -f html -o $(abspath $(REPORTDIR)lint/index.html))
	-@$(call yarn,lint --no-color | tee $(abspath $(REPORTDIR)lint.out))
endif


#=======================================================================================================================
# Testing
#=======================================================================================================================
ifndef NO_TEST_TARGET
.PHONY: test
test: $(call depends_on,$(TEST_DEPENDENCIES))
	-@$(call yarn,test,$(if $(UPDATE_SNAPSHOTS),-u))

.PHONY: test-coverage
test-coverage: $(call depends_on,$(TEST_DEPENDENCIES))
	@$(call yarn,test:coverage,--coverageDirectory=$(TESTDIR))

.PHONY: test-ci
test-ci: $(call depends_on,$(TEST_DEPENDENCIES)) | $(abspath $(REPORTDIR)coverage)/ $(abspath $(REPORTDIR)tests)/
	-@($(call yarn,test:coverage,--coverageDirectory=$(REPORTDIR)/coverage\
	                             --coverageReporters=html --coverageReporters=text-summary\
	                             --ci\
	                             --json)) | { tee /dev/fd/3 \
	                                          | grep -v '{.*}' > $(REPORTDIR)coverage.out ; \
	                                        } 3>&1 \
	                                      | jest-json-to-tap | { tee /dev/fd/4 \
	                                                             | tap-that-html > $(REPORTDIR)tests/index.html ; \
	                                                           } 4> $(REPORTDIR)tap.out
endif


#=======================================================================================================================
# Documentation
#=======================================================================================================================
ifndef NO_DOCS_TARGET
.PHONY: docs
$(call bundle_packages,docs,$(DOCDIR)vendor/,$(DOCS_BUNDLE_EXTRAS))
$(call bundle_dist_files,docs,$(DOCDIR)vendor/,$(DOCS_BUNDLE_DIST_FILES))
docs: $(call depends_on,$(DOCS_DEPENDENCIES))
docs:
	@echo "Creating documentation"
	@mkdocs build -q -f $(SRCDIR)mkdocs.yml -d $(DOCDIR)
endif


#=======================================================================================================================
# Distribution creation
#=======================================================================================================================
.PHONY: dist
ifndef NO_DIST_TARGET
$(call bundle_packages,dist,$(DISTDIR),$(DIST_BUNDLE_EXTRAS))
dist: $(addprefix $(DISTDIR),$(FILES))

$(addprefix $(DISTDIR),$(FILES)): $(OUTDIR).dist-dummy
$(OUTDIR).dist-dummy: $(addprefix $(SRCDIR),$(SRCS))
$(OUTDIR).dist-dummy: $(SRCDIR)rollup.config.js
$(OUTDIR).dist-dummy: $(call depends_on,$(DIST_DEPENDENCIES))
	@echo "Creating distribution packages"
	@cd $(SRCDIR) && $(call yarn,prepublish,--silent --environment outdir:$(DISTDIR)$(,)depdir:$(DEPDIR))
	@touch $@
endif


#=======================================================================================================================
# Helper targets
#=======================================================================================================================
$(DEPDIR)%:
	@echo "Installing dependency $* from $(DEP_REPO_$*)"
	$(call fetch_$(firstword $(subst :, ,$(DEP_REPO_$*))),$(DEP_REPO_$*),$(DEPDIR)/$*)

%/:
	-@mkdir -p $@
