(Mostly) current buildstatus:
[![Pipeline status](${SHIELDS_PIPELINE})](${REPO_BASEURL}/${REPO_USERNAME}/${PROJECT,,}/commits/master)
[![Tests](${SHIELDS_BASEURL}/tests.svg)](${REPO_SITE/test)
[![Coverage](${SHIELDS_BASEURL}/coverage.svg)](${REPO_SITE/coverage)
[![Style](${SHIELDS_BASEURL}/style.svg)](${REPO_SITE/lint)
[![Build](${SHIELDS_BASEURL}/build.svg)](${LATEST_TARBALL})<br/>
Please note the above shields are cached, and may represent the state of an old build.


${PROJECT}
$(printf '=%.0s' $(seq 1 ${#PROJECT}))
${DESCRIPTION}


Installation
------------
As you'll note I don't use [npm](https://npmjs.com) as it doesn't fit my
development process. This makes the installation a bit cumbersome. If
you create a proper `package.json` to submit this library to npm, I'll
happily [accept the pull-request](CONTRIBUTING.md). The current `package.json`
should suffice for most use cases.

1. Make sure you have to the latest version downloaded
   * [Download the latest release](${LATEST_TARBALL}).
   * Clone the repo:
     `git clone ${REPO_BASEURL}/${REPO_USERNAME}/${PROJECT,,}.git`.
2. Copy or move the relevant files from the `dist` directory to your own
   project.


Bug tracker
-----------
[Open a new issue](${ISSUE_URL}) for bugs
or feature requests. Please search for existing issues first.

Bugs or feature request will be fixed in the following order, if time
permits:

1. It has a pull-request with a working and tested fix.
2. It is easy to fix and has benefit to myself or a broader audience.
3. It puzzles me and triggers my curiosity to find a way to fix.


Contributing
------------
Anyone and everyone is welcome to [contribute](CONTRIBUTING.md).


License
-------
This software is licensed under the [ISC License](LICENSE).
