/* global process:false */

import buble from 'rollup-plugin-buble';
import butternut from 'rollup-plugin-butternut';
import includePaths from 'rollup-plugin-includepaths';
import pkg from './package.json';
import { dirname } from 'path';

const outdir = process.env.outdir || dirname(pkg.main || pkg.module || pkg['jsnext:main']);
const depdir = process.env.depdir ? process.env.depdir.split(':') : ['node_modules'];

export default {
	input: 'src/index.js',
	plugins: [
		buble(),
		butternut(),
		includePaths({ paths: depdir })
	],
	output: [
		{
			format: 'cjs',
			file: `${outdir}/${pkg.name}.cjs.min.js`,
			sourcemap: true
		},
		{
			format: 'iife',
			name: pkg.name,
			file: `${outdir}/${pkg.name}.iife.min.js`,
			sourcemap: true
		},
		{
			format: 'es',
			file: `${outdir}/${pkg.name}.es.min.js`,
			sourcemap: true
		}
	]
};
