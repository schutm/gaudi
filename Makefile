GAUDI_BUILD_DIR ?= .gaudi.build
GAUDI_BUILD_TMP_DIR ?= $(GAUDI_BUILD_DIR)/tmp/

GAUDI_DIR := $(dir $(lastword $(MAKEFILE_LIST)))
VPATH := $(GAUDI_DIR)

COMMON_FILES := LICENSE CONTRIBUTING.md README.md .editorconfig
GAUDI_FILES := Makefile targets.mk
VAGRANT_FILES := Vagrantfile provisioners/apt.provisioner

$(warning $(VAGRANT_ENVS))

bootstrap: $(COMMON_FILES) $(GAUDI_FILES) $(VAGRANT_FILES) $(VAGRANT_ENVS)

elixir: provisioners/apt.provisioner provisioners/apt_curl.provisioner provisioners/apt_elixir.provisioner provisioner/apt_inotify-tools.provisioners
node: provisioners/node.provisioner provisioner/apt_inotify-tools.provisioners provisioners/node_yarn.provisioner provisioners/node_yarn_packages.provisioner

phoenix: elixir node

provisioners/%: template/provisioners/% | provisioners/
	cp $< $@

Makefile: template/Makefile
	cp $< $@

targets.mk: template/targets.mk
	cp $< $@

$(GAUDI_BUILD_TMP_DIR)%: template/% gaudi.config
	@echo 'IFS=''; . ./gaudi.config ;\
	while read -r line ; do\
		while [[ "$$line" =~ (\$$\{[!#a-zA-Z_][a-zA-Z_0-9^,~%:/-+=?]*\}|\$$\(.+\)) ]] ; do\
			LHS=$${BASH_REMATCH[1]}\
			RHS="$$(eval echo "\"$$LHS\"")"\
			line=$${line//$$LHS/$$RHS} ;\
		done ;\
		echo -e "$$line" ;\
	done < $<' | bash > $@

%: $(GAUDI_BUILD_TMP_DIR)% | $(GAUDI_BUILD_TMP_DIR)
	@( [ ! -e $@ ] && cp $< $@ ) \
	|| ( diff $< $@ > /dev/null && touch $@ ) \
	|| ( diff $< $@ | less -Ps'New version available for $(subst .,\.,$@)\. Please press 'q' after you reviewed the changes$$' && \
	     while true; do\
			 	 read -p "(R)eplace with new version, (K)eep current version, (S)ave side-by-side? " answer;\
				 case $$answer in\
					 [rR]* ) cp $< $@; exit ;;\
					 [kK]* ) exit ;;\
					 [sS]* ) cp $< $@.new; exit ;;\
				 esac ;\
			 done\
		 )

%/:
	mkdir -p $@
