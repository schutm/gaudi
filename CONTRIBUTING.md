Contributing to Gaudi
=====================
Want to get involved? Thanks! Read on to see how you can help...


Bugs
----
A bug is a reproducible, demonstrable problem caused by the software.
Good bug reports are very important. Therefor some guidelines are
provided:

1. Check if the issue has already been reported using the **GitLab
   issue search**.

2. Check if the issue has not already been fixed by getting the latest
   `master` branch and try to reproduce the problem.

3. Create a minimal setup/example which reproduces the problem.

Please try to be as detailed as possible in the bug report. Add what OS
you're using, what arguments did you use to invoke the erroneous function
with?  What happened and what did you expect to happen?

**[File a bug report](https://gitlab.com/schutm/gaudi/issues)**


Pull requests
-------------
Good pull request - patches, improvements, new features - are even
better than good bug reports. Pull requests should remain focused in
scope and unrelated commits should be moved to separate pull requests.
Please open an issue to discuss a significant amount of work.

Although there are no hard coding guidelines to submit a pull request,
try to stay within the coding conventions of the source files. You might
want to check your code with the provided .eslint-file. Please update
any documentation that is relevant to the change you're making.

For pull requests follow this process:

1. Clone your fork, and configure the remotes:

   ```bash
   # Clone your fork of the repository into the current directory
   git clone https://gitlab.com/<your-username>/gaudi.git
   # Navigate to the newly cloned directory
   cd gaudi
   # Assigns the original repository to a remote called "upstream"
   git remote add upstream https://gitlab.com/schutm/gaudi.git
   ```

2. If you cloned a while ago, get the latest changes from upstream:

   ```bash
   git checkout master
   git pull upstream master
   ```

3. Create a new topic branch to contain your feature, change, or fix:

   ```bash
   git checkout -b <topic-branch-name>
   ```

4. Commit your changes in logical chunks. Please adhere to these [git
   commit message guidelines] (http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html).
   Use gits [interactive rebase](https://git-scm.com/book/en/v2/Git-Tools-Rewriting-History)
   feature to tidy up your commits before making them public.

5. Locally merge (or rebase) the upstream development branch into your
   topic branch:

   ```bash
   git pull [--rebase] upstream master
   ```

6. Push your topic branch up to your fork:

   ```bash
   git push origin <topic-branch-name>
   ```
7. Open a Pull Request with a clear title and description.
