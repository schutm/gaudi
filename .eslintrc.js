/* eslint quote-props: off, no-magic-numbers: off, max-lines: off */
/* global module */

module.exports = {
	'env': {
		'browser': true,
		'es6': true,
		'jest/globals': true
	},
	'plugins': ['jest'],
	'extends': 'eslint:recommended',
	'parserOptions': {
		'sourceType': 'module',
		'ecmaVersion': 8
	},
	'globals': {
		'm': false
	},
	'rules': {
		// Test rules
		'jest/no-disabled-tests': 'error',
		'jest/no-focused-tests': 'error',
		'jest/no-identical-title': 'error',

		// Possible Errors
		'no-await-in-loop': 'error',
		'no-cond-assign': ['error', 'except-parens'],
		'no-console': 'error',
		'no-constant-condition': ['error', { 'checkLoops': false }],
		'no-control-regex': 'error',
		'no-debugger': 'error',
		'no-dupe-args': 'error',
		'no-dupe-keys': 'error',
		'no-duplicate-case': 'error',
		'no-empty-character-class': 'error',
		'no-empty': ['error', { 'allowEmptyCatch': true }],
		'no-ex-assign': 'error',
		'no-extra-boolean-cast': 'error',
		'no-extra-parens': ['off', 'all', { 'conditionalAssign': true }],
		'no-extra-semi': 'error',
		'no-func-assign': 'error',
		'no-inner-declarations': ['error', 'both'],
		'no-invalid-regexp': 'error',
		'no-irregular-whitespace': ['error', {
			'skipStrings': true,
			'skipComments': false,
			'skipRegExps': false,
			'skipTemplates': true
		}],
		'no-obj-calls': 'error',
		'no-prototype-builtins': 'error',
		'no-regex-spaces': 'error',
		'no-sparse-arrays': 'error',
		'no-template-curly-in-string': 'error',
		'no-unexpected-multiline': 'error',
		'no-unreachable': 'error',
		'no-unsafe-finally': 'error',
		'no-unsafe-negation': 'error',
		'use-isnan': 'error',
		'valid-jsdoc': ['error', {
			'requireReturn': true,
			'requireParamDescription': true,
			'requireReturnDescription': true,
			'requireReturnType': false,
			'preferType': {}
		}],
		'valid-typeof': 'error',

		// Best Practices
		'accessor-pairs': ['error', {
			'setWithoutGet': true,
			'getWithoutSet': false
		}],
		'array-callback-return': 'error',
		'block-scoped-var': 'error',
		'class-methods-use-this': 'off',
		'complexity': ['error', 11],
		'consistent-return': ['error', { 'treatUndefinedAsUnspecified': false }],
		'curly': 'error',
		'default-case': ['error', { 'commentPattern': '/^no default$/i' }],
		'dot-location': ['error', 'property'],
		'dot-notation': ['error', {
			'allowKeywords': true
		}],
		'eqeqeq': ['error', 'always', { 'null': 'always' }],
		'guard-for-in': 'error',
		'no-alert': 'error',
		'no-caller': 'error',
		'no-case-declarations': 'error',
		'no-div-regex': 'off',
		'no-else-return': 'error',
		'no-empty-function': ['error', { 'allow': [] }],
		'no-empty-pattern': 'error',
		'no-eq-null': 'error',
		'no-eval': ['error', { 'allowIndirect': false }],
		'no-extend-native': ['error', { 'exceptions': [] }],
		'no-extra-bind': 'error',
		'no-extra-label': 'error',
		'no-fallthrough': ['error', { 'commentPattern': 'falls?\\s?through' }],
		'no-floating-decimal': 'error',
		'no-global-assign': ['error', { 'exceptions': [] }],
		'no-implicit-coercion': ['off', {
			boolean: false,
			number: true,
			string: true,
			allow: []
		}],
		'no-implicit-globals': 'error',
		'no-implied-eval': 'error',
		'no-invalid-this': 'error',
		'no-iterator': 'error',
		'no-labels': ['error', {
			'allowLoop': false,
			'allowSwitch': false
		}],
		'no-lone-blocks': 'error',
		'no-loop-func': 'error',
		'no-magic-numbers': ['error', {
			'detectObjects': false,
			'enforceConst': false,
			'ignore': [],
			'ignoreArrayIndexes': false
		}],
		'no-multi-spaces': ['error', { 'exceptions': {
			'Property': false,
			'BinaryExpression': false,
			'VariableDeclarator': false,
			'ImportDeclaration': false
		} }],
		'no-multi-str': 'error',
		'no-new-func': 'error',
		'no-new-wrappers': 'error',
		'no-new': 'error',
		'no-octal-escape': 'error',
		'no-octal': 'error',
		'no-param-reassign': ['error', { 'props': true }],
		'no-proto': 'error',
		'no-redeclare': ['error', { 'builtinGlobals': true }],
		'no-restricted-properties': ['error', {
			'object': 'arguments',
			'property': 'callee',
			'message': 'arguments.callee is deprecated'
		}, {
			'property': '__defineGetter__',
			'message': 'Please use Object.defineProperty instead.'
		}, {
			'property': '__defineSetter__',
			'message': 'Please use Object.defineProperty instead.'
		}, {
			'object': 'Math',
			'property': 'pow',
			'message': 'Use the exponentiation operator (**) instead.'
		}],
		'no-return-assign': ['error', 'except-parens'],
		'no-return-await': 'error',
		'no-script-url': 'error',
		'no-self-assign': ['error', { 'props': true }],
		'no-self-compare': 'error',
		'no-sequences': 'error',
		'no-throw-literal': 'error',
		'no-unmodified-loop-condition': 'error',
		'no-unused-expressions': ['error', {
			'allowShortCircuit': false,
			'allowTernary': false
		}],
		'no-unused-labels': 'error',
		'no-useless-call': 'error',
		'no-useless-concat': 'error',
		'no-useless-escape': 'error',
		'no-useless-return': 'error',
		'no-void': 'error',
		'no-warning-comments': 'off',
		'no-with': 'error',
		'prefer-promise-reject-errors': ['error', { 'allowEmptyReject': false }],
		'radix': ['error', 'always'],
		'require-await': 'error',
		'vars-on-top': 'error',
		'wrap-iife': ['error', 'inside', { 'functionPrototypeMethods': true }],
		'yoda': ['error', 'never'],

		// Strict Mode
		'strict': ['error', 'never'],

		// Variables
		'init-declarations': 'off',
		'no-catch-shadow': 'error',
		'no-delete-var': 'error',
		'no-label-var': 'error',
		'no-restricted-globals': 'off',
		'no-shadow-restricted-names': 'error',
		'no-shadow': ['error', {
			'builtinGlobals': true,
			'hoist': 'all',
			'allow': []
		}],
		'no-undef-init': 'error',
		'no-undef': ['error', { 'typeof': true }],
		'no-undefined': 'error',
		'no-unused-vars': ['error', {
			'vars': 'local',
			'args': 'all',
			'argsIgnorePattern': '^_'
		}],
		'no-use-before-define': ['error', 'nofunc'],

		// Stylistic Issues
		'array-bracket-spacing': ['error', 'never'],
		'block-spacing': ['error', 'always'],
		'brace-style': ['error', '1tbs', { 'allowSingleLine': true }],
		'camelcase': ['error', { 'properties': 'always' }],
		'capitalized-comments': ['error', 'always', {
			'line': {
				'ignoreInlineComments': false,
				'ignoreConsecutiveComments': true
			},
			'block': {
				'ignoreInlineComments': true,
				'ignoreConsecutiveComments': false
			}
		}],
		'comma-dangle': ['error', 'never'],
		'comma-spacing': ['error', { 'before': false, 'after': true }],
		'comma-style': ['error', 'last'],
		'computed-property-spacing': ['error', 'never'],
		'consistent-this': 'off',
		'eol-last': ['error', 'always'],
		'func-call-spacing': ['error', 'never'],
		'func-name-matching': ['off', 'always'],
		'func-names': 'error',
		'func-style': ['error', 'declaration', { allowArrowFunctions: true }],
		'id-blacklist': 'off',
		'id-length': 'off',
		'id-match': 'off',
		'indent': ['error', 'tab', {
			'SwitchCase': 0,
			'VariableDeclarator': 1,
			'outerIIFEBody': 1,
			'MemberExpression': 1,
			'FunctionDeclaration': {
				'parameters': 1,
				'body': 1
			},
			'FunctionExpression': {
				'parameters': 1,
				'body': 1
			},
			'CallExpression': {
				'arguments': 'first'
			},
			'ArrayExpression': 'first',
			'ObjectExpression': 'first'
		}],
		'jsx-quotes': ['off'],
		'key-spacing': ['error', {
			'beforeColon': false,
			'afterColon': true,
			'mode': 'strict'
		}],
		'keyword-spacing': ['error', {
			'before': true,
			'after': true,
			'overrides': {
				'return': { 'after': true },
				'throw': { 'after': true },
				'case': { 'after': true }
			}
		}],
		'line-comment-position': 'off',
		'linebreak-style': ['error', 'unix'],
		'lines-around-comment': 'off',
		'lines-around-directive': 'off',
		'max-depth': ['error', 4],
		'max-len': ['error', 100, 2, {
			'ignoreComments': false,
			'ignoreTrailingComments': false,
			'ignoreUrls': true,
			'ignoreRegExpLiterals': true,
			'ignoreStrings': true,
			'ignoreTemplateLiterals': true
		}],
		'max-lines': ['error', {
			'max': 300,
			'skipBlankLines': true,
			'skipComments': true
		}],
		'max-nested-callbacks': ['error', 5],
		'max-params': ['error', 5],
		'max-statements': ['error', 10],
		'max-statements-per-line': ['error', { max: 1 }],
		'multiline-ternary': 'off',
		'new-cap': ['error', {
			'newIsCap': true,
			'newIsCapExceptions': [],
			'capIsNew': false,
			'capIsNewExceptions': [],
			'properties': false
		}],
		'new-parens': 'error',
		'newline-after-var': 'off',
		'newline-before-return': 'error',
		'newline-per-chained-call': ['error', { 'ignoreChainWithDepth': 2 }],
		'no-array-constructor': 'error',
		'no-bitwise': 'error',
		'no-continue': 'error',
		'no-inline-comments': 'off',
		'no-lonely-if': 'error',
		'no-mixed-operators': ['error', {
			'groups': [
				['+', '-', '*', '/', '%', '**'],
				['&', '|', '^', '~', '<<', '>>', '>>>'],
				['==', '!=', '===', '!==', '>', '>=', '<', '<='],
				['&&', '||'],
				['in', 'instanceof']
			],
			'allowSamePrecedence': false
		}],
		'no-mixed-spaces-and-tabs': ['error', 'smart-tabs'],
		'no-multi-assign': 'error',
		'no-multiple-empty-lines': ['error', { 'max': 1, 'maxBOF': 0, 'maxEOF': 1 }],
		'no-negated-condition': 'off',
		'no-nested-ternary': 'error',
		'no-new-object': 'error',
		'no-plusplus': ['error', { 'allowForLoopAfterthoughts': true }],
		'no-restricted-syntax': 'off',
		'no-tabs': 'off',
		'no-ternary': 'off',
		'no-trailing-spaces': 'error',
		'no-underscore-dangle': ['error', { 'allowAfterThis': false, 'allowAfterSuper': false }],
		'no-unneeded-ternary': ['error', { 'defaultAssignment': false }],
		'no-whitespace-before-property': 'error',
		// TODO: enable once https://github.com/eslint/eslint/issues/6488 is resolved
		'object-curly-newline': ['off', {
			ObjectExpression: { minProperties: 0, multiline: true },
			ObjectPattern: { minProperties: 0, multiline: true }
		}],
		'object-curly-spacing': ['error', 'always'],
		'object-property-newline': ['error', {
			'allowMultiplePropertiesPerLine': true
		}],
		'one-var-declaration-per-line': ['error', 'always'],
		'one-var': ['error', 'never'],
		'operator-assignment': ['error', 'always'],
		'operator-linebreak': ['error', 'before', { 'overrides': { '?': 'before', ':': 'before' } }],
		'padded-blocks': ['error', 'never'],
		'quote-props': ['error', 'as-needed'],
		'quotes': ['error', 'single', { 'avoidEscape': true, 'allowTemplateLiterals': true }],
		'require-jsdoc': 'off',
		'semi-spacing': ['error', { 'before': false, 'after': true }],
		'semi': ['error', 'always'],
		'sort-keys': 'off',
		'sort-vars': 'off',
		'space-before-blocks': ['error', 'always'],
		'space-before-function-paren': ['error', {
			'anonymous': 'never',
			'named': 'never',
			'asyncArrow': 'always'
		}],
		'space-in-parens': ['error', 'never'],
		'space-infix-ops': 'error',
		'space-unary-ops': ['error', {
			'words': true,
			'nonwords': false,
			'overrides': {}
		}],
		'spaced-comment': ['error', 'always', {
			'line': {
				'exceptions': [],
				'markers': []
			},
			'block': {
				exceptions: [],
				markers: [],
				balanced: true
			}
		}],
		'unicode-bom': ['error', 'never'],
		'wrap-regex': 'off',

		// ECMAScript 6
		'arrow-body-style': ['error', 'as-needed', { 'requireReturnForObjectLiteral': true }],
		'arrow-parens': ['error', 'always'],
		'arrow-spacing': ['error', { 'before': true, 'after': true }],
		'constructor-super': 'error',
		'generator-star-spacing': ['error', { 'before': false, 'after': true }],
		'no-class-assign': 'error',
		'no-confusing-arrow': ['error', { 'allowParens': true }],
		'no-const-assign': 'error',
		'no-dupe-class-members': 'error',
		'no-duplicate-imports': 'error',
		'no-new-symbol': 'error',
		'no-restricted-imports': 'off',
		'no-this-before-super': 'error',
		'no-useless-computed-key': 'error',
		'no-useless-constructor': 'error',
		'no-useless-rename': ['error', {
			'ignoreDestructuring': false,
			'ignoreImport': false,
			'ignoreExport': false
		}],
		'no-var': 'error',
		'object-shorthand': ['error', 'always', {
			'ignoreConstructors': false,
			'avoidQuotes': true,
			'avoidExplicitReturnArrows': true
		}],
		'prefer-arrow-callback': ['error', {
			'allowNamedFunctions': false,
			'allowUnboundThis': false
		}],
		'prefer-const': ['error', {
			'destructuring': 'any',
			'ignoreReadBeforeAssign': false
		}],
		'prefer-numeric-literals': 'error',
		'prefer-rest-params': 'error',
		'prefer-spread': 'error',
		'prefer-template': 'error',
		'require-yield': 'error',
		'rest-spread-spacing': ['error', 'never'],
		'sort-imports': ['off', {
			'ignoreCase': false,
			'ignoreMemberSort': false,
			'memberSyntaxSortOrder': ['none', 'all', 'single', 'multiple']
		}],
		'symbol-description': 'error',
		'template-curly-spacing': ['error', 'never'],
		'yield-star-spacing': ['error', 'after']
	}
};
